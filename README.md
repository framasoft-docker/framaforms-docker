# Bootstrap Framaforms with Docker

## Description
This project provides the dockerfiles and docker-compose to easily spawn a [Framaforms](https://framagit.org/framasoft/framaforms) instance

## Run with Docker Compose
The fastest way to run this library locally is to use [Docker](https://docker.com).

 - [Install Docker](https://docs.docker.com/v17.12/install/)
 - Open a terminal
 - Git clone this repository (or alternatively download and unzip this package to a local directory on your machine)
    ```bash
    git clone https://framagit.org/framasoft-docker/framaforms-docker.git
    ```
 - Download the Framaforms SQL database dump
    ```bash
    curl -O https://framagit.org/framasoft/framaforms/-/raw/master/framaforms.sql
    ```
 - Launch the services (and wait for it all to build and boot)
    ```bash
    docker-compose up
    ```
 - Go to http://localhost:8888 in your browser.
 - The rest of the documentation, such as 1st time connexion and Framaforms features, is available either in the Framaforms [README.md](https://framagit.org/framasoft/framaforms/-/blob/master/README.md) or in the [wiki](https://framagit.org/framasoft/framaforms/-/wikis/Installing-Framaforms)

## Publishing the site

One way to publish the site based on these containers is to use a reverse proxy on the server hosting these containers.  
[Here is a good example](https://www.digitalocean.com/community/tutorials/how-to-configure-nginx-with-ssl-as-a-reverse-proxy-for-jenkins#step-one-%E2%80%94-configure-nginx) of Nginx reverse proxy configuration, complete with HTTPS, and HTTP to HTTPS redirection.  
Simply follow "Step One", and adapt to your domain name.

## Debug

Framaforms is based on Drupal. If you hever need to do some debugging, you can build the Framaforms image with the necessary debugging tool, namely Drush.
To do so, uncomment the 3 lines marked by a comment about debugging at the end of Dockerfile-Framaforms and force a rebuild of the image by typing the commands
```bash
docker-compose up --build
```
